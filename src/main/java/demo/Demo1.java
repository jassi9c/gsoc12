package demo;

import com.amazonaws.services.ec2.model.Instance;

public class Demo1 {

	public static void main(String[] args) {
		
		AmazonFreeTierPrototype demo = new AmazonFreeTierPrototype();
		
		Instance test1;
				
		// initialize connection to cloud
		if(demo.initCloud()==false){
			System.out.println("initCloud() failed.");
			System.exit(1);
		}
		
		System.out.println("Connection to Amazon Cloud initialized");
		
		// setup the dns server
		System.out.println("Setting up DNS Server.This may take a while.Please wait for 2 minutes.");
		if(demo.setupDNSServer()==false){
			System.out.println("setupDNS() failed.");
			System.exit(1);
		}

		System.out.println("DNS setup with internal ip = "+demo.dnsIP);
		System.out.println("Now going to setup an example instance with hostname : apache");
		
		// setup the test instance
		test1=(Instance)demo.setupInstance("apache");
		if(test1==null){
			System.out.println("Could not start / setup the new instance.");
			System.exit(1);
		}
		
		
		// print ip from api as well as nslookup
		System.out.println("Internal ip of apache from API : "+test1.getPrivateDnsName());
		System.out.println("Internal ip of apache from dnsServer through nslookup: \n"+demo.runLocalCommand("nslookup apache.emory.edu "+((Instance)demo.dnsServer).getPublicDnsName()));
		
		// stop and start the instance again
		System.out.println("Now stopping apache instance.");
		test1=(Instance)demo.stopInstance(test1);
		System.out.println("Sleeping for 5 seconds...");
		try {
			Thread.sleep(5*1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Starting apache again");
		test1=(Instance)demo.startInstance(test1);
		System.out.println("Internal ip of apache from API : "+test1.getPrivateDnsName());
		System.out.println("Internal ip of apache from dnsServer through nslookup: "+demo.runLocalCommand("nslookup apache.emory.edu "+((Instance)demo.dnsServer).getPublicDnsName()));
		
		System.out.println("Demo1 finished execution.\n");
		
	}

}
