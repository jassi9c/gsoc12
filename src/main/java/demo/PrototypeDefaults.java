package demo;

public abstract class PrototypeDefaults {

	String dnsIP ; // internal ip of the dnsServer 
	Object dnsServer;
	int runningInstances ; 
	
	public PrototypeDefaults(){
		dnsIP = "";
		dnsServer = null;
		runningInstances = 0;
	}
	
	abstract boolean initCloud();
	abstract  boolean cleanup();
	abstract  boolean setupDNSServer();
	abstract String setupDNSOnInstance(Object vm , String hostname);
	abstract String addInstanceIpToDNSServer(Object vm , String hostname);
	abstract Object launchInstance();
	abstract Object setupInstance(String hostname);
	abstract int checkNumberOfRunningInstances();
	abstract String extractIP(Object vm);
	abstract Object stopInstance(Object vm);
	abstract Object startInstance(Object vm);
	
}
