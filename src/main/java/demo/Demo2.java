package demo;

import com.amazonaws.services.ec2.model.Instance;

public class Demo2 {

	public static void main(String[] args) {
		
		long startTime = System.currentTimeMillis();
		
		AmazonFreeTierPrototype demo = new AmazonFreeTierPrototype();
		
		Instance test1;
		Instance test2;
				
		// initialize connection to cloud
		if(demo.initCloud()==false){
			System.out.println("initCloud() failed.");
			System.exit(1);
		}
		
		System.out.println("Connection to Amazon Cloud initialized");
		
		// setup the dns server
		System.out.println("Setting up DNS Server.This may take a while.Please wait for 2 minutes.");
		if(demo.setupDNSServer()==false){
			System.out.println("setupDNS() failed.");
			System.exit(1);
		}

		System.out.println("DNS setup with internal ip = "+demo.dnsIP);
		
		System.out.println("Now going to setup an example instance with hostname : test1");
		// setup the test1 instance
		test1=(Instance)demo.setupInstance("test1");
		if(test1==null){
			System.out.println("Could not start / setup the new instance.");
			System.exit(1);
		}
		System.out.println("Internal ip of apache from API : "+test1.getPrivateDnsName());
		
		System.out.println("Now going to setup the second example instance with hostname : test2");
		// setup the test2 instance
		test2=(Instance)demo.setupInstance("test2");
		if(test2==null){
			System.out.println("Could not start / setup the new instance.");
			System.exit(1);
		}
		System.out.println("Internal ip of apache from API : "+test2.getPrivateDnsName());
		
		System.out.println("Will now ping test2.emory.edu from test1.emory.edu");
		demo.runRemoteCommandWrapper("gsoc_dns", "ping -c 3 test2.emory.edu", "ec2-user", test1.getPublicDnsName(), false, true);
		
		System.out.println("Will now ping test1.emory.edu from test2.emory.edu");
		demo.runRemoteCommandWrapper("gsoc_dns", "ping -c 3 test1.emory.edu", "ec2-user", test2.getPublicDnsName(), false, true);

		System.out.println("Execution finished in "+(System.currentTimeMillis()-startTime)/1000+" seconds");
	}

}
