package demo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.StartInstancesRequest;
import com.amazonaws.services.ec2.model.StartInstancesResult;
import com.amazonaws.services.ec2.model.StopInstancesRequest;
import com.amazonaws.services.ec2.model.StopInstancesResult;
import com.amazonaws.services.ec2.model.Tag;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class AmazonFreeTierPrototype extends PrototypeDefaults {

	private AmazonEC2      	ec2;
	private Hashtable		id_hostname;
		
	public AmazonFreeTierPrototype() {
		super();
		id_hostname = new Hashtable(10);
	}
	
	public void getDNSServer(){
		DescribeInstancesResult describeInstancesRequest = ec2.describeInstances();
		List<Reservation> reservations = describeInstancesRequest.getReservations();
		Set<Instance> instances = new HashSet<Instance>();

		for (Reservation reservation : reservations) {
			instances.addAll(reservation.getInstances());
		}
		for(Instance i : instances){
			if(i.getState().getName().equals("running")){
				this.dnsServer = i;
				this.dnsIP = extractIP(i);
			}
		}
	}
	
	public String runLocalCommand(String command) {
		String output = "";
		try{
    		Process child = Runtime.getRuntime().exec(command);
    	    InputStream in = child.getInputStream();
    	    int c;
    	    while ((c = in.read()) != -1) {
    	        output+=((char)c);
    	    }
    	}catch(Exception e){
    		return null;
    	}
    	return output;
    }
	
	@Override
	public boolean initCloud() {
		try
		{
			AWSCredentials credentials = new PropertiesCredentials(
				AmazonFreeTierPrototype.class.getResourceAsStream("AwsCredentials.properties"));
		 	ec2 = new AmazonEC2Client(credentials);
		 	
		 	// the below statement verifies if the credentials are correct.
		 	// incorrect credentials will raise an exception
		 	DescribeInstancesResult describeInstancesRequest = ec2.describeInstances();
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
			//e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean cleanup() {
		return false;
	}

	@Override
	public boolean setupDNSServer() {

		if(dnsServer!=null){
			System.out.println("DNS has been set already , with ip = "+dnsIP+". Terminate and restart.");
			return false;
		}
		Instance newDNS = launchInstance();  
		if((dnsIP=extractIP(newDNS))==null){
			System.out.println("Could not launch instance.");
			return false;			
		}
		//System.out.println("dnsIP set to "+dnsIP);
		String[] files = {"dummy" , "emory.edu" , "named.conf" , "setup_dns_server.sh" };
		for(String file : files)
			if(scpFileWrapper("gsoc_dns",file,"ec2-user",newDNS.getPublicDnsName())==null)
				return false;
		if(runRemoteCommandWrapper("gsoc_dns","sh setup_dns_server.sh","ec2-user",newDNS.getPublicDnsName(),false,false)==null)
			return false;
		dnsServer = newDNS;
		id_hostname.put(((Instance)dnsServer).getImageId(), "dnsServer");
		return true;
	}

	@Override
	public Instance launchInstance() {
		Instance ni;
		try {
		RunInstancesRequest runInstancesRequest = new RunInstancesRequest()
	    .withInstanceType("t1.micro")
	    .withImageId("ami-e565ba8c")
	    .withMinCount(1)
	    .withMaxCount(1)
	    .withSecurityGroupIds("test")
	    .withKeyName("gsoc_dns"); 
		
		RunInstancesResult runInstances = ec2.runInstances(runInstancesRequest);
		Instance newVM = runInstances.getReservation().getInstances().get(0);

		DescribeInstancesRequest di = new DescribeInstancesRequest().withInstanceIds(newVM.getInstanceId());
		DescribeInstancesResult  dr = ec2.describeInstances(di);
		ni = dr.getReservations().get(0).getInstances().get(0);
		
		while(ni.getState().getName().equals("pending")){
			Thread.sleep(10*000);	// sleep for 10 seconds
			dr = ec2.describeInstances(di);
			ni = dr.getReservations().get(0).getInstances().get(0);
			if(ni.getState().getName().equals("running")){
				break;
			}
		}
		
		//System.out.println("VM has been launched successfully with internal ip = "+ni.getPrivateDnsName());
		}catch(Exception e){
			System.out.println("Could not launch the instance. Exception occured.");
			e.printStackTrace();
			return null;
		}
		return ni;
	}

	@Override
	public int checkNumberOfRunningInstances() {
		DescribeInstancesResult describeInstancesRequest = ec2.describeInstances();
        List<Reservation> reservations = describeInstancesRequest.getReservations();
        Set<Instance> instances = new HashSet<Instance>();

        for (Reservation reservation : reservations) {
            instances.addAll(reservation.getInstances());
        }
        int numI = 0;
        for(Instance i : instances)
        	if(i.getState().getName().equals("running"))
        		numI++;
		return numI;
	}	
	
	public String runRemoteCommandWrapper(String key , String command , String user , String host , boolean getStatus , boolean showOutput) {
		//System.out.println(command+"\t"+host);
		String result = null;
		int tries = 0;
		while(tries<5 && result == null){
			result = runRemoteCommand(key , command ,  user ,  host , getStatus , showOutput);
			try{
				if(result==null)
					Thread.sleep(15*1000);
			}catch(Exception e){
				//System.out.println("Exception : "+e.getMessage());
				//System.out.println("Will try again in 10 seconds");
			}
			tries++;		
		}
		return  result;
	}
	
	public String scpFileWrapper(String key , String file , String user , String host) {
		String result = null;
		int tries = 0;
		while ( tries < 5 && result == null){
			result = scpFile(key , file ,user , host);
			try{
				//System.out.println("#Try="+tries+" Waiting for 17 seconds before trying again...");
				if(result==null)
					Thread.sleep(17*1000);
				}catch(Exception e){}
			tries++;
			
		}
		return result;	
	}
	
	@SuppressWarnings("static-access")
	public String runRemoteCommand(String key , String command , String user , String host , boolean getStatus , boolean showOutput){
		//System.out.println(command+"\t"+host);
		String output = null ;
		try
		{
		JSch jsch=new JSch();
		//System.out.println(System.getProperty("user.dir") + "/src/demo/dns_files/"+key+".pem");
		jsch.addIdentity(System.getProperty("user.dir") + "/src/main/java/demo/dns_files/"+key+".pem");
		jsch.setConfig("StrictHostKeyChecking","no");
		Session session=jsch.getSession(user, host, 22);
		session.connect();
		
		Channel channel=session.openChannel("exec");
		((ChannelExec)channel).setPty(true);
		((ChannelExec)channel).setCommand(command);
		channel.setInputStream(null);
		((ChannelExec)channel).setErrStream(System.err);
		InputStream in=channel.getInputStream();
		channel.connect();
		byte[] tmp=new byte[1024];
		while(true){
			while(in.available()>0){
				int i=in.read(tmp, 0, 1024);
				if(i<0)
					break;
				if(output==null)
					output = new String(tmp, 0, i);
				else
					output += new String(tmp, 0, i) ;
				if(showOutput)
					System.out.print(new String(tmp, 0, i));
			}
			if(channel.isClosed()){
				if(getStatus)
					output+=channel.getExitStatus();
				///System.out.println("exit-status: "+channel.getExitStatus());
				break;
			}
			try{Thread.sleep(1000);}catch(Exception ee){}
		}
		channel.disconnect();
		session.disconnect();
		
		}catch(Exception e){
			//System.out.println("Exception. "+e.getMessage());
			return null;
		}
		return output;		
	}
	
	//@SuppressWarnings("static-access")
	public String scpFile(String key , String file , String user , String host) {
		String output = null ;
		try{
	    FileInputStream fis=null;
		JSch jsch=new JSch();
		//System.out.println(key + "\t"+ file + "\t"+user+"\t"+host);
		jsch.addIdentity(System.getProperty("user.dir") + "/src/main/java/demo/dns_files/gsoc_dns.pem");
		String filePath = System.getProperty("user.dir") + "/src/main/java/demo/dns_files/"+file;
		jsch.setConfig("StrictHostKeyChecking","no");
		Session session=jsch.getSession(user, host, 22);
		session.connect();	
        String command="scp -t "+file;
        Channel channel=session.openChannel("exec");
        ((ChannelExec)channel).setCommand(command);
        InputStream in=channel.getInputStream();
        OutputStream out=channel.getOutputStream();
        //InputStream in=channel.getInputStream();
        channel.connect();
        File _lfile = new File(filePath);
        long filesize=_lfile.length();
        
        command="C0644 "+filesize+" ";
        if(file.lastIndexOf('/')>0){
          command+=file.substring(file.lastIndexOf('/')+1);
        }
        else{
          command+=file;
        }
        command+="\n";
        //System.out.println(command);
        out.write(command.getBytes()); 
        out.flush();
        if(checkAck(in)!=0){
        	System.exit(0);
        }
        // send a content of lfile
        fis=new FileInputStream(filePath);
        byte[] buf=new byte[1024];
        while(true){
        	int len=fis.read(buf, 0, buf.length);
        	if(len<=0) 
        		break;
        	out.write(buf, 0, len); out.flush();
        	if(checkAck(in)!=0){
        		System.exit(0);
        	}
        }
        fis.close();
        fis=null;
        // send '\0'
        buf[0]=0; out.write(buf, 0, 1); out.flush();
        if(checkAck(in)!=0){
        	System.exit(0);
        }
        out.close();
        channel.disconnect();
        session.disconnect();
        //System.out.println("exit-status: "+channel.getExitStatus());
        output = Integer.toString(channel.getExitStatus());
		}catch(Exception e){
			//System.out.println("Exception in scp : "+e.getMessage());
			return null;
		}
		return output;		
	}

	@Override
	public String setupDNSOnInstance(Object ni , String hostname) {
		Instance vm = (Instance)ni;
		return runRemoteCommandWrapper("gsoc_dns", "sudo sed -i 's:nameserver [0-9\\.]*:nameserver "+dnsIP.replace(".", "\\.")+":' /etc/resolv.conf", "ec2-user",vm.getPublicDnsName(),true ,false);
	}

	@Override
	public Object setupInstance(String hostname) {
		Instance ni = launchInstance();
		//CreateTagsRequest createTagsRequest = new CreateTagsRequest();
		//createTagsRequest.withResources(ni.getInstanceId()).withTags(new Tag("hostname", hostname));
		//ec2.createTags(createTagsRequest);
		//List<Tag> tags = ni.getTags() ;
		//System.out.println("Number of tags = "+tags.size());
		//return ni;
			
		if(setupDNSOnInstance(ni , hostname)!=null && addInstanceIpToDNSServer(ni , hostname)!=null) 
			if(reloadDNSServer()==null?false:true)
			{
				id_hostname.put(ni.getInstanceId(), hostname);
				return ni;
			}
		return null;
		
		
	}

	@Override
	public String addInstanceIpToDNSServer(Object o , String hostname) {
		//System.out.println("addInstanceIpToDNSServer() called");
		Instance vm = (Instance)o;
		String vmIP = extractIP(vm);
		//System.out.println("the private ip is"+vmIP);
		return this.runRemoteCommandWrapper("gsoc_dns", "sudo sed -i '$a "+hostname.replace(".", "\\.")+" A "+vmIP.replace(".", "\\.")+"' /var/named/emory.edu", "ec2-user", ((Instance)dnsServer).getPublicDnsName(),true,false);
	}
	
	public String extractIP(Object vm){

		String ipStr = ((Instance)vm).getPrivateDnsName();
		Pattern p = Pattern.compile("ip-(\\d{1,3}-\\d{1,3}-\\d{1,3}-\\d{1,3}).ec2.internal");
		Matcher m = p.matcher(ipStr);
		if(m.find())
			return m.group(1).replace("-", ".");
		else {	// the private dns name is not in the usual ip-xx-xx-xx-xx-ec2.internal form but in domU.-xx-xx-xx-xx-x form . 
				// so , login and get the ip from vm
			return this.runRemoteCommandWrapper("gsoc_dns", "sudo ifconfig eth0 | perl -n -e 'if (m/inet addr:([\\d\\.]+)/g) { print $1 }'", "ec2-user", ((Instance)vm).getPublicDnsName(),false,false);
			
		}
	}
	
	public String reloadDNSServer(){
		return this.runRemoteCommandWrapper("gsoc_dns", "sudo service named reload", "ec2-user", ((Instance)dnsServer).getPublicDnsName(),true,false);
	}

	@Override
	public Object stopInstance(Object vm) {
		Instance instance = (Instance)vm;
		try{
			String vmip = extractIP(instance);
			StopInstancesRequest stopReq = new StopInstancesRequest().withInstanceIds(instance.getInstanceId());
			StopInstancesResult  stopRes = ec2.stopInstances(stopReq);
			vm = waitForTransition(instance,"stopped");
			/*
			List<Tag> tags = instance.getTags() ;
			System.out.println("Number of tags = "+tags.size());
			Tag t = tags.get(0);
			*/
			runRemoteCommandWrapper("gsoc_dns","sudo sed -i '/"+id_hostname.get(instance.getInstanceId())+" A "+vmip.replace(".", "\\.")+"/d' /var/named/emory.edu","ec2-user",((Instance)dnsServer).getPublicDnsName(),true,false);
			reloadDNSServer();
			
		}catch(Exception e){
			System.out.println("Could not stop instance");
			e.printStackTrace();
			return null;
		}
		return vm;
	}

	public Instance waitForTransition(Instance vm , String newState) {
		Instance ni = null;
		try{
			DescribeInstancesRequest di = new DescribeInstancesRequest().withInstanceIds(vm.getInstanceId());
			DescribeInstancesResult  dr = ec2.describeInstances(di);
			ni = dr.getReservations().get(0).getInstances().get(0);

			while(!ni.getState().getName().equals(newState)){
				Thread.sleep(5*000);	// sleep for 10 seconds
				dr = ec2.describeInstances(di);
				ni = dr.getReservations().get(0).getInstances().get(0);				
			}
		}catch(Exception e){}
		return ni;
	}
	
	
	@Override
	public Object startInstance(Object vm) {

		Instance instance = (Instance)vm;
		try{
			StartInstancesRequest startReq = new StartInstancesRequest().withInstanceIds(instance.getInstanceId());
			StartInstancesResult  startRes = ec2.startInstances(startReq);
			vm = waitForTransition(instance,"running");
			String vmIP = extractIP(vm);
			//Tag t = instance.getTags().get(0);
			this.runRemoteCommandWrapper("gsoc_dns", "sudo sed -i '$a "+id_hostname.get(instance.getInstanceId())+" A "+vmIP.replace(".", "\\.")+"' /var/named/emory.edu", "ec2-user", ((Instance)dnsServer).getPublicDnsName(),true,false);
			reloadDNSServer();
		}catch(Exception e){
			return null;
		}
		return vm;
	}

	static int checkAck(InputStream in) throws IOException{
	    int b=in.read();
	    // b may be 0 for success,
	    //          1 for error,
	    //          2 for fatal error,
	    //          -1
	    if(b==0) return b;
	    if(b==-1) return b;

	    if(b==1 || b==2){
	      StringBuffer sb=new StringBuffer();
	      int c;
	      do {
		c=in.read();
		sb.append((char)c);
	      }
	      while(c!='\n');
	      if(b==1){ // error
		System.out.print(sb.toString());
	      }
	      if(b==2){ // fatal error
		System.out.print(sb.toString());
	      }
	    }
	    return b;
	  }
	
}
