package prometheus;

import com.amazonaws.util.json.JSONArray;

import net.sf.json.JSONObject;

public class LaunchInstances implements Runnable {

	private Cloud cloud;
	private JSONObject request;
	private RemoteExecutionResponse result;
	private VirtualMachine vm;
	private String[] status;
	private int presentStatus;
	private boolean errorOccurred;
	private String errorMessage;
	
	public LaunchInstances(JSONObject request , Cloud cloud) throws Exception{	
		this.cloud=cloud;
		this.request=request;
		this.vm = null;
		this.result = new RemoteExecutionResponse();
		presentStatus = 0;
		status= new String[] {
				"Not yet started",
				"Launching vm",
				"Waiting for vm to boot up and allow ssh",
				"Over-riding the hostname",
				"Over-riding the /etc/resolv.conf entries",
				"Adding the {hostname,ip} to the nameservers",
				"Executing the startup_script",
				"Going to put tags on the vm" ,
				"Finished."
		};
		errorOccurred=false;
	}
	
	public String getStatus(){
		if(errorOccurred)
			return errorMessage;
		return status[presentStatus];
	}
	
	public RemoteExecutionResponse getResult(){
		return result;
	}
	
	public int getTotalNumberOfStatus(){
		return status.length;
	}
	
	public JSONObject getRequest(){
		return request;
	}
	
	public void run() {
	
		try {
			String hostname = request.getString("hostname");
			//status="launching vm";
			presentStatus++;
			System.out.println(hostname+": launching vm");
			vm = cloud.launchVM(request);
			vm.setKeyName(request.getString("sshKey"));
			cloud.addVirtualMachine(hostname, vm);
			cloud.getVirtualMachine(hostname).setStatus("booting up....");
			System.out.println(hostname+": vm launched successfully.");
			
			result.output="Summary :\nThe VM was launched successfully.\n";
			result.returnStatus=0;
			
			//status="waiting for vm to boot up and allow ssh";
			presentStatus++;
			System.out.println(hostname+": waiting for vm to boot up and allow ssh");
			
			WaitForVmToBootUp waitReq = new WaitForVmToBootUp(cloud,request);
			RemoteExecutionResponse waitReqResponse = waitReq.execute();
			if(checkResponse(waitReqResponse)){
				cloud.removeVirtualMachine(hostname);
				errorOccurred=true;
				errorMessage="Time out. Could not ssh into the vm.";
				result.output+="Time out. Could not ssh into the vm.";
				result.returnStatus=-1;
				return;
			}
			
			cloud.getVirtualMachine(hostname).setStatus("being configured....");
			
			//status="over-riding the hostname";
			presentStatus++;
			System.out.println(hostname+": over-riding the hostname");
			
			OverrideHostnameOfInstance setHostname = new OverrideHostnameOfInstance(cloud,request);
			RemoteExecutionResponse setHostnameReqResponse = setHostname.execute();
			if(checkResponse(setHostnameReqResponse)){
				cloud.removeVirtualMachine(hostname);
				errorOccurred=true;
				errorMessage="Hostname of the vm could not be set.\nThe error code is "+setHostnameReqResponse.getReturnStatus()+" and output is "+setHostnameReqResponse.getOutput();
				System.out.println(hostname+"Hostname of the vm could not be set.\nThe error code is "+setHostnameReqResponse.getReturnStatus()+" and output is "+setHostnameReqResponse.getOutput());
				result.output+="Hostname of the vm could not be set.\nThe error code is "+setHostnameReqResponse.getReturnStatus()+" and output is "+setHostnameReqResponse.getOutput();
				result.returnStatus=setHostnameReqResponse.getReturnStatus();
				return;
			}
			
			result.output+="The hostname \""+hostname+"\" was successfully set on the new vm.\n";			
			
			//System.out.println(hostname+": hostname set.");
			
			presentStatus++;
			if(request.containsKey("overrideDNS")){
				
				//status="over-riding the /etc/resolv.conf entries";
				
				System.out.println(hostname+": over-riding the /etc/resolv.conf entries");
				
				OverrideDNSOfInstance overrideDNSReq = new OverrideDNSOfInstance(cloud,request);
				RemoteExecutionResponse overrideDNSReqResponse = overrideDNSReq.execute();
				if(checkResponse(overrideDNSReqResponse)){
					cloud.removeVirtualMachine(hostname);
					errorOccurred=true;
					errorMessage="The /etc/resolv.conf entries could not be set.\nThe error code is "+overrideDNSReqResponse.getReturnStatus()+" and output is "+overrideDNSReqResponse.getOutput();
					System.out.println(hostname+": The /etc/resolv.conf entries could not be set.\nThe error code is "+overrideDNSReqResponse.getReturnStatus()+" and output is "+overrideDNSReqResponse.getOutput());
					result.output+="The /etc/resolv.conf entries could not be set.\nThe error code is "+overrideDNSReqResponse.getReturnStatus()+" and output is "+overrideDNSReqResponse.getOutput();
					result.returnStatus=overrideDNSReqResponse.getReturnStatus();
					return;
				}
				
				result.output+="The /etc/resolv.conf entries were updated successfully.\n";
			}
			
			
			
			presentStatus++;
			if(request.containsKey("addEntryToDNSServers")){
				
				//status="adding the {hostname,ip} to the nameservers";
				
				System.out.println(hostname+": adding the {hostname,ip} to the nameservers");
				
				AddEntryToDNSServers addEntryReq = new AddEntryToDNSServers(cloud,request);
				RemoteExecutionResponse  addEntryReqResponse = addEntryReq.execute();
				if(checkResponse(addEntryReqResponse)){
					cloud.removeVirtualMachine(hostname);
					errorOccurred=true;
					errorMessage="The /etc/resolv.conf entries could not be set.\nThe error code is "+addEntryReqResponse.getReturnStatus()+" and output is "+addEntryReqResponse.getOutput();
					System.out.println(hostname+": The /etc/resolv.conf entries could not be set.\nThe error code is "+addEntryReqResponse.getReturnStatus()+" and output is "+addEntryReqResponse.getOutput());
					result.output+="The /etc/resolv.conf entries could not be set.\nThe error code is "+addEntryReqResponse.getReturnStatus()+" and output is "+addEntryReqResponse.getOutput();
					result.returnStatus=addEntryReqResponse.getReturnStatus();
					return;
				}
				
				result.output+="The ip of the vm was successfully added to the dns servers.\n";
			}
			
			
			presentStatus++;
			if(request.containsKey("startup_script")){
				
				
				String filePath = request.getString("startup_script") ;
				filePath = filePath.substring(filePath.lastIndexOf("/"));
				
				request.put("sourceFile", request.get("startup_script"));
				request.put("targetFile" , "~/"+filePath);
				
				//status="executing the startup_script";
				
				System.out.println(hostname+": executing the startup_script");
				
				RunScriptOnInstance runScript = new RunScriptOnInstance(cloud , request);
				RemoteExecutionResponse runScriptRes = runScript.execute();
				if(checkResponse(runScriptRes)){
					errorOccurred=true;
					errorMessage="The startup script could not be executed.\nThe error code is "+runScriptRes.getReturnStatus()+" and output is "+runScriptRes.getOutput();
					System.out.println(hostname+": The startup script could not be executed.The error code is "+runScriptRes.getReturnStatus()+" and output is \n"+runScriptRes.getOutput());
					result.output+="The startup script could not be executed.\nThe error code is "+runScriptRes.getReturnStatus()+" and output is "+runScriptRes.getOutput();
					result.returnStatus=runScriptRes.getReturnStatus();
					return;
				}
				else{
					result.output+="The output for script \""+request.getString("startup_script").substring(request.getString("startup_script").lastIndexOf("/"))+"\" on host \""+hostname+"\"is : \n"+runScriptRes.getOutput()+"\n";
				}
			}
			
			System.out.println(hostname+": all cloudjobs for this vm done");
			
			JSONObject tags = new JSONObject();
			tags.put("hostname", hostname);
			if(request.containsKey("Name"))
				tags.put("Name", request.getString("Name"));
			else
				tags.put("Name", hostname);
			
			if(request.containsKey("overrideDNS")){
				String dnsservers = "";
				 
				net.sf.json.JSONArray temp = request.getJSONArray("overrideDNS");
				String dns[] = new String[temp.size()];
				for(int i=0;i<temp.size();i++){
					dnsservers+=temp.getString(i)+";";
					dns[i]=temp.getString(i);
				}
				cloud.getVirtualMachine(hostname).setListOfDNSServers(dns);
				tags.put("dnsServers", dnsservers);
			}
			else
				tags.put("dnsServers", "NA");
			
			if(request.containsKey("isDNSServer")){
				if(request.getString("isDNSServer").replace("\n","").equals("Yes")){
					tags.put("isDNSServer", "Yes");
					//cloud.setDNSServer(hostname, cloud.getVirtualMachine(hostname).getPublicIp());
					cloud.getVirtualMachine(hostname).setAsDNSServer(true);
				}
			}
						
			//status="Going to put tags on the vm";
			presentStatus++;
			System.out.println("Going to put tags on the vm.");
			if(cloud.setTags(cloud.getVirtualMachine(hostname), tags)==false){
				errorOccurred=true;
				errorMessage="Could not put tags on the vm in cloud";
				System.out.println("Could not put tags");
				result.output+="Could not put tags on the vm in cloud";
				result.returnStatus=-1;
			}else{
				cloud.getVirtualMachine(hostname).setTags(tags);
				System.out.println("Tags set on the vm");
			}
			
			cloud.getVirtualMachine(hostname).setStatus("running");
			
			//status="done";
			presentStatus++;
			result.output="Setup done for new vm : "+hostname + "\n" + result.output;
			
		} catch (Exception e) {
			System.out.println("Exception occurred in LaunchInstances class: ");
			System.out.println(e.getMessage());
			errorOccurred=true;
			errorMessage=e.getMessage();
			e.printStackTrace();
		}
		
	}
	
	private boolean checkResponse(RemoteExecutionResponse response){
		
		if(response.returnStatus!=0){
			System.out.println("Error : ");
			System.out.println("Shell response : \n"+response.getOutput());
			return true;
		}
		return false;
		
	}

}
