package prometheus;

import net.sf.json.JSONException;
import net.sf.json.JSONObject;

public class RunScriptOnInstance extends CloudJob{

	private String ip;
	
	public RunScriptOnInstance(Cloud cloud , JSONObject details) throws Exception{
		super(cloud,details);
		this.ip = cloud.getVirtualMachine(details.getString("hostname")).getPublicIp();;
	}
	
	@SuppressWarnings("unused")
	@Override
	public RemoteExecutionResponse execute() throws Exception {
		
		String sourceFile;
		String targetFile;
		try{
			sourceFile = details.getString("sourceFile");
			targetFile = details.getString("targetFile");
			
			details.put("host", ip);
			
		}catch(JSONException je){
			throw new JSONException("sourceFile/targetFile missing in json object.");
		}
		
		
		
		if(cloud.uploadFile(details)==false)
			throw new Exception("Could not upload the script to vm");
		
		boolean runAsRoot = false; 
		try{
			runAsRoot = details.getBoolean("runAsRoot");
		}catch(JSONException je){}
		
		String command = "chmod +x "+targetFile;
		if(runAsRoot)
			command="sudo "+command;
		details.put("command", command);
		RemoteExecutionResponse changePermissionResponse = new RemoteExecutionResponse();
		changePermissionResponse = cloud.runRemoteCommand(details);
		
		if(changePermissionResponse.getReturnStatus()!=0)
			throw new Exception("Could not execute chmod +x on the script. Error Code : "+changePermissionResponse.getReturnStatus());
		
		command = "";
		if(runAsRoot)
			command="sudo ";
		command+=targetFile;
		details.put("command", command);		
		return cloud.runRemoteCommand(details);
	}

}
