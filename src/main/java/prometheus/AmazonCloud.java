package prometheus;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.json.JSONException;
import net.sf.json.JSONObject;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.DeleteTagsRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.DescribeKeyPairsResult;
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.KeyPairInfo;
import com.amazonaws.services.ec2.model.RebootInstancesRequest;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.SecurityGroup;
import com.amazonaws.services.ec2.model.StartInstancesRequest;
import com.amazonaws.services.ec2.model.StartInstancesResult;
import com.amazonaws.services.ec2.model.StopInstancesRequest;
import com.amazonaws.services.ec2.model.StopInstancesResult;
import com.amazonaws.services.ec2.model.Tag;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;
import com.amazonaws.services.ec2.model.TerminateInstancesResult;

public class AmazonCloud extends Cloud {

	private AmazonEC2      	ec2;

	private List<String> securityGroups;
	private List<String> sshKeys;
	
	public AmazonCloud(){
		super();
		ec2 = null;		
		securityGroups=new ArrayList<String>();
		sshKeys = new ArrayList<String>();
	}
		
	public AmazonCloud(String credentialsFile) throws Exception,NullPointerException{
		super();
		securityGroups=new ArrayList<String>();
		sshKeys = new ArrayList<String>();
		init(credentialsFile);		
	}
	
	public String[] getKeyPairNames(){
		String[] temp = new String[sshKeys.size()];
		temp = sshKeys.toArray(temp);
		return temp;
	}
	
	public String[] getSecurityGroups(){
		String[] temp = new String[securityGroups.size()];
		temp = securityGroups.toArray(temp);
		return temp;
	}
	
	protected boolean loadMetadataOfExistingVMs() throws Exception{
		try{
			
		DescribeKeyPairsResult keyp =   ec2.describeKeyPairs();
		List<KeyPairInfo> kpinfo = keyp.getKeyPairs(); 
		for(KeyPairInfo kp : kpinfo){
			//System.out.println(kp.getKeyName());
			if(sshKeys==null)
				System.out.println("sshKeys is null");
			sshKeys.add(kp.getKeyName());
		}
		
		DescribeSecurityGroupsResult secr = ec2.describeSecurityGroups();
		List<SecurityGroup> scinfo = secr.getSecurityGroups();
		for(SecurityGroup group : scinfo){
			//System.out.println(group.getGroupName());
			securityGroups.add(group.getGroupName());
		}
			
		DescribeInstancesRequest describeInstancesRequest = new DescribeInstancesRequest();
		DescribeInstancesResult  describeInstancesResult = ec2.describeInstances(describeInstancesRequest);
		List<Reservation> reservations = describeInstancesResult.getReservations();
		
		for(Reservation reservation : reservations){
			List<Instance> instances = reservation.getInstances();
			for(Instance vm : instances){
				
				// Terminated / VMs being terminated are not needed
				if(vm.getState().getName().equals("terminated") || vm.getState().getName().equals("shutting-down") )
					continue;
				
				VirtualMachine temp = new VirtualMachine();
				temp.setVM(vm);
				Map<String , String > tags = getTags(temp);
				
				if(tags==null || tags.size()==0 ){
					continue;
				}
						
				String requiredTags[] = { "hostname" , "dnsServers"};
				
				boolean skip = false;
				for(String requiredTag : requiredTags){
					if(!tags.containsKey(requiredTag) || tags.get(requiredTag)==(null) ||tags.get(requiredTag).equals("")){
						skip=true;
						break;
					}
				}
				
				if(skip)
					continue;
				
				
				
				VirtualMachine newVM = new VirtualMachine();
				newVM.setHostname((String)tags.get("hostname"));
				newVM.setVM(vm);
				newVM.setTags(tags);
				newVM.setKeyName(vm.getKeyName());
				newVM.setListOfDNSServers(((String)tags.get("dnsServers")).split(";"));
				
				if(vm.getState().getName().equals("running")){
					newVM.setPrivateIp(extractInternalIP(newVM, null));
					newVM.setPublicIp(vm.getPublicIpAddress());
					newVM.setStatus("running");
				}else{
					newVM.setStatus("stopped");
				}
				if(tags.containsKey("isDNSServer") && tags.get("isDNSServer").equals("Yes")){
					newVM.setAsDNSServer(true);
				}
				addVirtualMachine((String)tags.get("hostname"), newVM);
			}
		}
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return true;
	}
	
	protected void updateMetadataVM(VirtualMachine newVM , String sshKeyPath , String hostname) throws Exception{
		try{
			Instance vm = (Instance)newVM.getVM();
			String external_ip = vm.getPublicIpAddress();
			String internal_ip = extractInternalIP(newVM, new String[] {sshKeyPath,  external_ip});
			virtualMachines.get(hostname).setPrivateIp(internal_ip);
			virtualMachines.get(hostname).setPublicIp(external_ip);
			virtualMachines.get(hostname).setVM(vm);
			
			Map<String,String> oldTags = getTags(newVM);
			
			JSONObject newtags = new JSONObject();
			newtags.put("hostname", hostname);
			if(oldTags.containsKey("Name"))
				newtags.put("Name", oldTags.get("Name"));
			
			deleteTags(newVM);
			setTags(newVM, newtags);
		}catch(Exception e){
			throw new Exception(e.getMessage());
		}
		
	}
	
	@Override
	public boolean init(String credentialsFile) throws Exception{
		try
		{
			//System.out.println(new File(credentialsFile).getCanonicalPath());
			AWSCredentials credentials = new PropertiesCredentials(new FileInputStream(new File(credentialsFile).getCanonicalFile()));
		 	ec2 = new AmazonEC2Client(credentials);	
		 	// The below statement verifies if the credentials are correct.
		 	// Incorrect credentials will raise an exception
		 	loadMetadataOfExistingVMs();
		 	if(virtualMachines.size()==0){
		 		System.out.println("Info. : No vms were loaded in the framework.\n");
		 	}
		 }
		catch(NullPointerException np){
			throw new NullPointerException("credentials file passed is null");
		}
		catch(FileNotFoundException fnf){
			throw new FileNotFoundException("File not found / incorrect permissions");
		}
		catch(AmazonClientException ace) {
			throw new AmazonClientException("Invalid Credentials");
		}
		catch(Exception e) {
			System.out.println("An exception occured during the init. process.");
			System.out.println(e.getMessage());
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		cloud=ec2;
		return true;
	}

	@Override
	public Object getCredentials() {
		// Yet to be implemented
		return null;
	}

	@Override
	public VirtualMachine launchVM(JSONObject vmDetails) throws Exception{
		Instance newInstance = null;
		String instanceType ;
		String imageID ; 
		String securityGroup; 
		String sshKeyPath;
		String sshKey;
		try{
			instanceType = vmDetails.getString("type");
			imageID = vmDetails.getString("ami");
			securityGroup = vmDetails.getString("securityGroup") ;
			sshKeyPath = vmDetails.getString("sshKeyPath") ;
			sshKey = vmDetails.getString("sshKey") ;
		}catch(JSONException je){
			throw new JSONException("Json Object is missing some attibutes...");
		}
		try {
			RunInstancesRequest runInstancesRequest = new RunInstancesRequest()
			.withInstanceType(instanceType)
			.withImageId(imageID)
			.withMinCount(1)
			.withMaxCount(1)
			.withSecurityGroupIds(securityGroup)
			.withKeyName(sshKey);

			RunInstancesResult runInstances = ec2.runInstances(runInstancesRequest);
			Instance newVM = runInstances.getReservation().getInstances().get(0);

			DescribeInstancesRequest di = null;
			
			int attempt=0;
			int limit = 3;
			// in rare cases , need to try again because ec2 may not have yet 'started' the allocation
			while(di==null && attempt<limit){
				try{
					di= new DescribeInstancesRequest().withInstanceIds(newVM.getInstanceId());
					attempt++;
				}catch(Exception e){}
			}
			
			DescribeInstancesResult  dr = ec2.describeInstances(di);
			newInstance = dr.getReservations().get(0).getInstances().get(0);

			while(newInstance.getState().getName().equals("pending")){
				Thread.sleep(3*000);	
				dr = ec2.describeInstances(di);
				newInstance = dr.getReservations().get(0).getInstances().get(0);
				if(newInstance.getState().getName().equals("running")){
					break;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception(e.getMessage());		
		}
		
		VirtualMachine newVM = new VirtualMachine();
		newVM.setVM(newInstance);
		newVM.setPublicIp(newInstance.getPublicIpAddress());
		String args[] = new String[2];
		args[0]=sshKeyPath;
		args[1]=newInstance.getPublicIpAddress();
		String internal_ip = extractInternalIP(newVM , args );
		if(!internal_ip.equals(null)){
			newVM.setPrivateIp(internal_ip);
		}
		else
			throw new Exception("Could not extract the internal ip of the new vm");
		
		newVM.setKeyName(vmDetails.getString("sshKey"));
		newVM.setHostname(vmDetails.getString("hostname"));
		
		return newVM;
	}

	@SuppressWarnings("unused")
	@Override
	public VirtualMachine startVM(String hostname) throws Exception{

		Instance newInstance = null;
		try{
			String instanceId = ((Instance)virtualMachines.get(hostname).getVM()).getInstanceId();
			StartInstancesRequest startRequest = new StartInstancesRequest().withInstanceIds(instanceId);
			StartInstancesResult startResult = ec2.startInstances(startRequest);

			DescribeInstancesRequest di = new DescribeInstancesRequest().withInstanceIds(instanceId);
			DescribeInstancesResult  dr = ec2.describeInstances(di);
			newInstance = dr.getReservations().get(0).getInstances().get(0);

			while(newInstance.getState().getName().equals("pending")){
				Thread.sleep(3*000);	
				dr = ec2.describeInstances(di);
				newInstance = dr.getReservations().get(0).getInstances().get(0);
				if(newInstance.getState().getName().equals("running")){
					break;
				}
			}
			virtualMachines.get(hostname).setVM(newInstance);
			virtualMachines.get(hostname).setPublicIp(newInstance.getPublicIpAddress());
			virtualMachines.get(hostname).setPrivateIp(extractInternalIP(virtualMachines.get(hostname), null));
			virtualMachines.get(hostname).setStatus("running");
			
		}catch(Exception e){
			throw new Exception(e.getMessage());
		}
		return virtualMachines.get(hostname);
	}

	@SuppressWarnings("unused")
	@Override
	public VirtualMachine stopVM(String hostname) throws Exception{
		Instance newInstance = null;
		try{
			String instanceId = ((Instance)virtualMachines.get(hostname).getVM()).getInstanceId();

			StopInstancesRequest stopRequest = new StopInstancesRequest().withInstanceIds(instanceId);
		    StopInstancesResult stopResult = ec2.stopInstances(stopRequest);

			DescribeInstancesRequest di = new DescribeInstancesRequest().withInstanceIds(instanceId);
			DescribeInstancesResult  dr = ec2.describeInstances(di);
			newInstance = dr.getReservations().get(0).getInstances().get(0);

			while(newInstance.getState().getName().equals("stopping")){
				Thread.sleep(3*000);	
				dr = ec2.describeInstances(di);
				newInstance = dr.getReservations().get(0).getInstances().get(0);
				if(newInstance.getState().getName().equals("stopped")){
					break;
				}
			}
			virtualMachines.get(hostname).setPublicIp(null);
			virtualMachines.get(hostname).setPrivateIp(null);
			virtualMachines.get(hostname).setStatus("stopped");
		}catch(Exception e){
			throw new Exception(e.getMessage());
		}
		return virtualMachines.get(hostname);
	}
	
	@SuppressWarnings("unused")
	public void terminateVM(String hostname) throws Exception{
		Instance newInstance = null;
		try{
			String instanceId = ((Instance)virtualMachines.get(hostname).getVM()).getInstanceId();

			TerminateInstancesRequest terminateReq = new TerminateInstancesRequest().withInstanceIds(instanceId);
			TerminateInstancesResult terminateReqResult = ec2.terminateInstances(terminateReq);

			DescribeInstancesRequest di = new DescribeInstancesRequest().withInstanceIds(instanceId);
			DescribeInstancesResult  dr = ec2.describeInstances(di);
			newInstance = dr.getReservations().get(0).getInstances().get(0);

			while(newInstance.getState().getName().equals("shutting-down")){
				Thread.sleep(3*000);	
				dr = ec2.describeInstances(di);
				newInstance = dr.getReservations().get(0).getInstances().get(0);
				if(newInstance.getState().getName().equals("terminated")){
					break;
				}
			}
			
			removeVirtualMachine(hostname);
			
		}catch(Exception e){
			throw new Exception(e.getMessage());
		}
		return;		
	}
	
	@Override
	protected String extractInternalIP(VirtualMachine vme , String[] args) throws Exception{
		Instance vm = (Instance)vme.getVM();
		String ipStr = vm.getPrivateDnsName();
		Pattern p = Pattern.compile("ip-(\\d{1,3}-\\d{1,3}-\\d{1,3}-\\d{1,3}).ec2.internal");
		Matcher m = p.matcher(ipStr);
		if(m.find()){
			//System.out.println("returning 1: "+m.group(1).replace("-", "."));
			return m.group(1).replace("-", ".");
		}
		else{
			//System.out.println(args[1] +" "+((Instance)vm).getPrivateIpAddress());
			//System.out.println("returning 2: "+vm.getPrivateIpAddress());
			if(vm.getPrivateIpAddress()!=null)
				return vm.getPrivateIpAddress();
		}
		/* 
		 * Seems that getPrivateIpAddress() works when getPrivateDnsName() is not in usual format
		 * 
		 */
		JSONObject details = new JSONObject();
		try{
			details.put("username", "ec2-user");
			details.put("sshKeyPath", "/home/jaspal/gsoc_demo/gsoc12_jaspal.pem");
			details.put("host", vm.getPublicIpAddress());
			details.put("command", "/sbin/ifconfig eth0 | grep \"inet addr\" | awk -F: '{print $2}' | awk '{print $1}'");
			RemoteExecutionResponse response = new RemoteExecutionResponse();
			response = super.runRemoteCommand(details);
			if(response.returnStatus==0)
				return response.output;
			else
				throw new Exception("Could not extract the internal ip");
		}catch(Exception e){
			throw new Exception(e.getMessage());
		}
		
	}
	
	@SuppressWarnings("rawtypes")
	public boolean setTags(VirtualMachine vm , JSONObject tags){
		try{
			List<Tag> vmtags = new ArrayList<Tag>();
			Iterator keys = tags.keys();
			while(keys.hasNext()){
				Tag t = new Tag();
				t.setKey((String)keys.next());
				t.setValue(tags.getString(t.getKey()));
				vmtags.add(t);
			}
			CreateTagsRequest ctr = new CreateTagsRequest();
			ctr.setTags(vmtags);
			ctr.withResources(((Instance)vm.getVM()).getInstanceId());
			ec2.createTags(ctr);
			return true;
		}catch(Exception e){
			System.out.println("Exception occurred in setTags() for hostname : "+vm.getHostname());
			System.out.println(vm.getHostname()+"  "+e.getMessage());
			e.printStackTrace();
		}
		return false;
	}
	
	public void deleteTags(VirtualMachine vm) throws Exception{
		DeleteTagsRequest ctr = new DeleteTagsRequest();
		ctr.withResources(((Instance)vm.getVM()).getInstanceId());
		ec2.deleteTags(ctr);
		return ;
	}
	
	public Map<String,String> getTags(VirtualMachine instance){
		Instance vm = (Instance)instance.getVM();
		List<Tag> tags =  vm.getTags();
		Map<String,String> vmtags = new HashMap<String, String>();
		for(Tag t : tags){
			vmtags.put((String)t.getKey(), (String)t.getValue());
		}
		return vmtags;
	}
	
	public void rebootVM(String hostname) throws Exception{
		Instance newInstance = null;
		try{
			String instanceId = ((Instance)virtualMachines.get(hostname).getVM()).getInstanceId();
			RebootInstancesRequest rebootReq = new RebootInstancesRequest().withInstanceIds(instanceId);
		
			ec2.rebootInstances(rebootReq);
			/*
			DescribeInstancesRequest di = new DescribeInstancesRequest().withInstanceIds(instanceId);
			DescribeInstancesResult  dr = ec2.describeInstances(di);
			newInstance = dr.getReservations().get(0).getInstances().get(0);

			
			int i=0;
			
			//while(!newInstance.getState().getName().equals("running")){
			while(i<10){
				System.out.println("rebooting...in loop");
				System.out.println("state is "+newInstance.getState().getName());
				Thread.sleep(3*000);	
				dr = ec2.describeInstances(di);
				newInstance = dr.getReservations().get(0).getInstances().get(0);
				i++;
				//if(newInstance.getState().getName().equals("running")){
				//	break;
				//}
			}
			
			System.out.println("reboot done...");
			*/
			//virtualMachines.get(hostname).setStatus("running");
			
		}catch(Exception e){
			throw new Exception(e.getMessage());
		}
		return;		

	}
}
