package prometheus;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class AddEntryToDNSServers extends CloudJob{

	private String ipToBeAdded;
	
	public AddEntryToDNSServers(Cloud cloud , JSONObject details ) throws Exception {
		super(cloud, details);
		if(!details.containsKey("addEntryToDNSServers"))
			throw new Exception("AddEntryToDNSServers CloudJob called but with missing json object");
		this.ipToBeAdded=cloud.getVirtualMachine(details.getString("hostname")).getPrivateIp().replace("\n", "");
	}
	
	@Override
	public RemoteExecutionResponse execute() throws Exception {
		
		RemoteExecutionResponse response = null;
		
		try{
		
			JSONArray listOfDNSServers = details.getJSONArray("addEntryToDNSServers"); 
			for(int i=0;i<listOfDNSServers.size();i++){
				JSONObject serverDetails = listOfDNSServers.getJSONObject(i);
				//String command = "sudo sed -i \"\\$a"+details.getString("hostname").replace(".", "\\.")+"\\ A\\ "+ipToBeAdded.replace(".", "\\.")+"\" "+serverDetails.getString("dnsFile");
				
				String command = "sudo grep \""+details.getString("hostname")+"\" "+serverDetails.getString("dnsFile") + " -n ";	
				details.put("command", command);
				details.put("host" , cloud.getDNSServerPublicIP(serverDetails.getString("dnsServer")));
				response = cloud.runRemoteCommand(details);
				
				if(response.getReturnStatus()==1){ // grep returns 1 if no match is found	
					if(response.getOutput().equals("")){						
						command = "sudo sh -c 'echo \""+details.getString("hostname")+". A "+ipToBeAdded+"\" >> "+serverDetails.getString("dnsFile")+"'";
						details.put("command", command);
						response = cloud.runRemoteCommand(details);						
					}
					else{
						System.out.println("Output should have been \"\" , instead it is  : \n"+response.getOutput());
					}
				}else{
					
					int lineToBeDeleted = Integer.parseInt(response.getOutput().substring(0,response.getOutput().lastIndexOf(":")));
					command = "sudo sed -i "+lineToBeDeleted+"d "+serverDetails.getString("dnsFile");
					command += "; sudo sh -c 'echo \""+details.getString("hostname")+". A "+ipToBeAdded+"\" >> "+serverDetails.getString("dnsFile")+"'";
					//command = "sudo sed -i 's:"+details.getString("hostname").replace(".", "\\.")+"\\.\\sA\\s[0-9\\.]*$:"+details.getString("hostname").replace(".", "\\.")+"\\. A "+ipToBeAdded.replace(".", "\\.")+":' " +serverDetails.getString("dnsFile");
					//System.out.println("Executing \n"+command);
					details.put("command", command);
					response = cloud.runRemoteCommand(details);
				}
				
				if(response.getReturnStatus()==0){
					details.put("command", "sudo service named restart");
					response = cloud.runRemoteCommand(details);
				}
				else{
					System.out.println("Executed \n"+command);
					System.out.println("Exit status : " + response.getReturnStatus());
					System.out.println("Exception occurred : \n"+response.getOutput());
				}

				
			}
			
		}catch(Exception e){
			throw new Exception(e.getMessage());			
		}
		
		return response;
	}

}
