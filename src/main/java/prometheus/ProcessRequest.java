package prometheus;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.io.IOUtils;


public class ProcessRequest implements Runnable {

	private JSONArray request;
	private boolean launchReqPresent;
	private boolean remoteExeReqPresent;
	private String requestType ;  
	private static Cloud cloud = null;
	
	private static String credentialsFile;
	private static String requestFile;
	
	private ProcessRequest(){
		this.request=null;
		this.launchReqPresent=false;
		this.remoteExeReqPresent=false;
		this.requestType = null;
		cloud = null;
	}
	
	private ProcessRequest(JSONArray req , String requestType) throws Exception{
		this.request = req;		
		this.requestType = requestType;
		if(cloud==null){
			
			cloud = new AmazonCloud(credentialsFile);
		}
	}

	@SuppressWarnings("unused")
	public void run() {

		JSONObject temp;
		int seq_number;

		ArrayList<LaunchInstances> launchReqs = null ;
		ArrayList<RemoteExecutionRequest> remoteExeReqs = null;
		
		ArrayList<String> newVMs = new ArrayList<String>();
		
		
		ArrayList< ArrayList<JSONObject> > parallelLaunchRequests = new ArrayList< ArrayList<JSONObject> >();
		
		try {
			for(int i=1 ; i<=request.size() ; i++){
				ArrayList<JSONObject> al = new ArrayList<JSONObject>();
				for(int j=0 ; j<request.size() ; j++ ){
					temp = request.getJSONObject(j);
					try{
						seq_number = Integer.parseInt(temp.getString("seq"));
					}catch(Exception e){
						throw new Exception(e.getMessage());
					}
					if(seq_number==i)
						al.add(temp);
				}
				//System.out.println("adding al with size = "+al.size()+" to parallelLReq.");
				if(al.size()>0)
					parallelLaunchRequests.add(al);
			}
			if(requestType.equals("launchReq"))
				launchReqs = new ArrayList<LaunchInstances>();
			else
				remoteExeReqs = new ArrayList<RemoteExecutionRequest>();
			
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		
		//System.out.println("Total size of parallel requests : "+parallelLaunchRequests.size());
		
		
		try{

			for(int i=0 ; i<parallelLaunchRequests.size() ; i++){
				//System.out.println("i="+i);
				@SuppressWarnings({ "unchecked", "rawtypes" })
				ArrayList<JSONObject> parallelReq = (ArrayList)parallelLaunchRequests.get(i);
				Thread[] threads = new Thread[parallelReq.size()];
				if(parallelReq.size()>1)
				{
					for(int j=0 ; j<threads.length;j++)
					{
						if(requestType.equals("launchReq"))
						{
							JSONObject temp2 = (JSONObject)parallelReq.get(j);
							launchReqs.add(new LaunchInstances((JSONObject)parallelReq.get(j),cloud));
							threads[j] = new Thread(launchReqs.get(j));
						}else if(requestType.equals("remoteExeReq"))
						{
							threads[j] = new Thread(new RemoteExecutionRequest((JSONObject)parallelReq.get(j) , cloud));
						}
					}
					for(Thread t : threads)
						t.start();
					for(Thread t : threads)
						t.join();
				}else{
					if(requestType.equals("launchReq")){
						//System.out.println("executing single vm");
						launchReqs.add(new LaunchInstances((JSONObject)parallelReq.get(0),cloud));
						LaunchInstances launchSingleVM = launchReqs.get(0);
						launchSingleVM.run();
						//System.out.println("execution of single vm done.");
					}else{
						RemoteExecutionRequest executeSingleReq = new RemoteExecutionRequest((JSONObject)parallelReq.get(0) , cloud);
						executeSingleReq.run();
					}
				}
				
				if(requestType.equals("launchReq"))
				{
					//System.out.println("\n Summary of the newly launched vms : \n");
					//System.out.format("%50s\t%28s\t%28s\n","hostname","public_ip","private_ip");
					
					for(LaunchInstances launchReq : launchReqs ){
						RemoteExecutionResponse response = launchReq.getResult();
						if(response.getReturnStatus()!=0){
							System.out.println("There were some errors during the launch of the new vm with hostname : "+launchReq.getRequest().getString("hostname"));
							//System.out.println("The partial output is :\n");
							//System.out.println(response.getOutput());
							//JSONObject req = launchReq.getRequest();
							//System.out.println("The orginal request was : "+req.toString());
						}else{
							JSONObject req = launchReq.getRequest();
							String hostname = req.getString("hostname");
							//System.out.println("addind "+hostname+" to newVMs");
							newVMs.add(hostname);
							//Object vm = cloud.getVM(hostname);
							//System.out.format("%50s\t%28s\t%28s\n", hostname , cloud.getExternalIP(vm) ,cloud.getInternalIP(vm));
						}
					}
					launchReqs.clear();
				}
				//System.out.println("i="+i+" done");
			}
			
		//System.out.println("i done");
		if(requestType.equals("launchReq")){
			//System.out.println("Size of newVMs = "+newVMs.size());
			System.out.println("\nSummary of the newly launched vms : \n");
			System.out.format("%40s\t%28s\t%28s\n","hostname","public_ip","private_ip");
			for(String hostname : newVMs){
				//System.out.println("querying "+hostname);
				//Object vm = cloud.getVM(hostname);
				System.out.format("%40s\t%28s\t%28s\n", hostname , cloud.getVirtualMachine(hostname).getPublicIp() ,cloud.getVirtualMachine(hostname).getPrivateIp());
			}
		}
			
		}catch(Exception e){
			e.printStackTrace();
			System.out.println(e.toString());
			System.out.println("Exception while processing request :\n"+e.getMessage());
		}
	}

	
	@SuppressWarnings("static-access")
	public static void main(String[] args){
		
	    CommandLineParser parser = new PosixParser();
	    Options options = new Options();
	    Option credentialsOption = OptionBuilder.withArgName("credentialsFile")
	    						.hasArg()
	    						.withDescription("the credentials file used to connect to cloud - required")
	    						.create("c");
	    Option requestOption =  OptionBuilder.withArgName("requestFile")
								.hasArg()
								.withDescription("the request file containing the requests - required")
								.create("r");
	    		
	    options.addOption(requestOption);
	    options.addOption(credentialsOption);
	    
	    CommandLine line = null; 
	    try{
	    	line = parser.parse(options, args);
	    }catch(ParseException exp){
	        System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
	    }

	    if(!line.hasOption("c") || !line.hasOption("r")){
	    	HelpFormatter formatter = new HelpFormatter();
	    	formatter.printHelp( "java -jar gsoc12.jar", options );
	    	System.exit(1);
	    }
	    
	    ProcessRequest processRequest = new ProcessRequest();
	    
	    credentialsFile = line.getOptionValue("c");
	    requestFile = line.getOptionValue("r");
		
		JSONObject json = processRequest.getJSONRequestFromFile();
		if(processRequest.validateJSONRequest(json)==false){
			System.out.println("Json request is not proper.");
			System.exit(1);
		}
			
		System.out.println("Processing the request. Please wait..\n");
		processRequest.processJSONRequest(json);		
	}
	
	private boolean processJSONRequest(JSONObject json){
		
		Thread[] threads = new Thread[2];
		try{
			if(launchReqPresent){
				threads[0] = new Thread(new ProcessRequest(json.getJSONArray("LaunchInstances") , "launchReq" ));
				threads[0].start();
			}
			if(remoteExeReqPresent){
				threads[1] = new Thread(new ProcessRequest(json.getJSONArray("RemoteExecutions") , "remoteExeReq"));
				threads[1].start();
			}

			if(launchReqPresent)
				threads[0].join();
			if(remoteExeReqPresent)
				threads[1].join();
		}catch(Exception e){
			System.out.println("Exception in processJSONRequest : "+e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;	
	}
	
	private boolean validateJSONRequest(JSONObject json){

		
		JSONArray launchInstances = null;
		JSONArray remoteExecutions = null;
		
		try{
			launchInstances = json.getJSONArray("LaunchInstances");
			launchReqPresent=true;
		}catch(JSONException je){
			//System.out.println("Could not find any LaunchInstances request");
		}
		
		try{
			remoteExecutions = json.getJSONArray("RemoteExecutions");
			remoteExeReqPresent=true;
		}catch(JSONException je){
			//System.out.println("Could not find any RemoteExecutions request");
		}
		
		if(launchInstances==null && remoteExecutions==null){
			System.out.println("Could not find LaunchInstances jsonarray or RemoteExecutions jsonarray");
			return false;
		}

		boolean result1 = false;
		boolean result2 = false;
		
		if(launchInstances!=null)
			result1 =  validateLaunchInstancesJSONRequest(launchInstances);
		else
			result1 = true;
		
		if(remoteExecutions!=null)
			result2 = validateRemoteExecutionsJSONRequest(remoteExecutions);
		else
			result2 = true;
		
		if(!result1 || !result2)
			return false;
		
		return true;			
	}
	
	private boolean validateLaunchInstancesJSONRequest(JSONArray launchInstances){
		
		int arraySize = launchInstances.size();
		JSONObject req ;
		
		String requiredParameters[] = { "seq",
										"ami",
										"username",
										"sshKey",
										"sshKeyPath",
										"type",
										"hostname",
										"securityGroup"
										};
		
		String temp = ""; 
		for(int i=0 ; i<arraySize ; i++){
			req = launchInstances.getJSONObject(i);
			try{
				temp = req.getString("seq");
				@SuppressWarnings("unused")
				int k = Integer.parseInt(temp);
			}catch(JSONException je){
				System.out.println("Parameter \""+requiredParameters[0]+"\" missing in this request :");
				System.out.println(req.toString());
				return false;
			}catch(Exception e){
				System.out.println("seq should be a number , instead it is \""+temp+"\"");
				return false;
			}
						
			for(int j=0; j<requiredParameters.length;j++){
				try{
					temp = req.getString(requiredParameters[j]);
				}catch(JSONException je){
					System.out.println("Parameter \""+requiredParameters[j]+"\" missing in this request :");
					System.out.println(req.toString());
					return false;
				}
			}
		}
		return true;
	}
	
	@SuppressWarnings("unused")
	private boolean validateRemoteExecutionsJSONRequest(JSONArray remoteExecutions){
		
		int arraySize = remoteExecutions.size();
		JSONObject req ;
		
		String requiredParameters[] = { "seq",										
										"type"
										};
		
		String seq = ""; 
		String type = "";
		for(int i=0 ; i<arraySize ; i++){
			
			req = remoteExecutions.getJSONObject(i);
			try{
				seq = req.getString("seq");
				type = req.getString("type");
				int k = Integer.parseInt(seq);
			}catch(JSONException je){
				System.out.println("Parameter \"seq\"/\"type\" missing in this request :");
				System.out.println(req.toString());
				return false;
			}catch(NumberFormatException nfe){
				System.out.println("seq should be a number , instead it is \""+seq+"\"");
				return false;
			}
			
			if(type.equals("start_vm") || type.equals("stop_vm") || type.equals("terminate_vm")){
				try{
					String hostname = req.getString("hostname");
				}catch(JSONException je){
					System.out.println("hostname missing in this request : " + req.toString());
					return false;
				}
			}else {
				try{
					String username = req.getString("username");
					String hostname		= req.getString("hostname");
					String sshKeyPath = req.getString("sshKeyPath");
				}catch(JSONException je){
					System.out.println("host/username/sshKeyPath missing in this request : " + req.toString());
					return false;
				}
			}
		}
		return true;
	}
	
	private JSONObject getJSONRequestFromFile(){
		
		FileInputStream is = null;
		
			try {
				is = new FileInputStream(new File( requestFile ).getCanonicalFile());
			}catch (IOException ioe) {
				System.out.println("Could not find the file. Check path and permissions of file.");
				System.exit(1);
			}
		
		
		String jsonTxt = null;
		JSONObject result = null;
		try {
			jsonTxt = IOUtils.toString( is );
			result = (JSONObject) JSONSerializer.toJSON(jsonTxt);; 
		}catch (Exception e) {
			System.out.println("Could not convert the contents of file to json object.");
			System.out.println("The error message is : "+e.getMessage().substring(0,e.getMessage().lastIndexOf(jsonTxt)));
			System.exit(1);
		}
		return result;
	}

		
}
