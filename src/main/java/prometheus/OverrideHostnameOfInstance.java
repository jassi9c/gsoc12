package prometheus;

import net.sf.json.JSONObject;

public class OverrideHostnameOfInstance extends CloudJob{

	private String ip;
	
	public OverrideHostnameOfInstance(Cloud cloud , JSONObject details) throws Exception{
		super(cloud, details);
		this.ip=cloud.getVirtualMachine(details.getString("hostname")).getPublicIp();
	}
	
	@Override
	public RemoteExecutionResponse execute() throws Exception {
		
		try{
			
			String hostname = details.getString("hostname");
			String command ="sudo hostname "+hostname.replace(".", "\\.")+" ; sudo sed -i 's:HOSTNAME [A-Za-z\\.]*:HOSTNAME "+hostname.replace(".", "\\.")+":' /etc/sysconfig/network";
			details.put("command", command);
			details.put("host", ip);
			
			//System.out.println("excuting command to over-ride hostname");
			
			return cloud.runRemoteCommand(details);			
		}catch(Exception e){
			throw new Exception(e.getMessage() + "\nThe JSON Object is :\n"+details.toString());
		}
	}
}
