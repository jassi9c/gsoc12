package prometheus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;

public abstract class Cloud {

	Object cloud ;							
	private SSHLibrary sshL ;				// not to be exposed to sub-classes
	Map<String , String> dnsServers_name_ip_map ; 
	Map<String , VirtualMachine> virtualMachines;
	
	private int SSH_TRIES_LIMIT = 20;
	private int INTERVAL_BETWEEN_SSH_ATTEMPTS = 10; // interval in secondss
	
	public Cloud(){
		cloud = null;
		sshL = new JschSSHLibrary();
		virtualMachines = new HashMap<String,VirtualMachine>();
	}
	
	public Collection<VirtualMachine> getAllVirtualMachines(){
		return virtualMachines.values();
	}
	protected void addVirtualMachine(String hostname , VirtualMachine vm){
		virtualMachines.put(hostname, vm);
	}
	protected void removeVirtualMachine(String hostname){
		virtualMachines.remove(hostname);
	}
	public VirtualMachine getVirtualMachine(String hostname){
		return virtualMachines.get(hostname);
	}
	public String getDNSServerPublicIP(String hostname){
		if(virtualMachines.containsKey(hostname) && virtualMachines.get(hostname).isThisADNSServer())
			return virtualMachines.get(hostname).getPublicIp();
		return null;
	}
	
	public ArrayList<VirtualMachine> getAllOnlineDNSServers(){
		ArrayList<VirtualMachine> dnsvms = new ArrayList<VirtualMachine>();
		for(Map.Entry<String, VirtualMachine> entry : virtualMachines.entrySet()){
			if(entry.getValue().isThisADNSServer() && entry.getValue().getStatus().equals("running")){
				dnsvms.add(entry.getValue());
			}
		}
		return dnsvms;
	}
	
	public abstract boolean init(String credentialsFile) throws Exception;
	public abstract Object getCredentials();
	public boolean loadNewCredentials(String credentialsFile) throws Exception{
		return init(credentialsFile);
	} 
	
	public abstract VirtualMachine launchVM(JSONObject vmDetails) throws Exception;
	public abstract VirtualMachine startVM(String hostname) throws Exception;
	public abstract VirtualMachine stopVM(String hostname) throws Exception;
	public abstract void terminateVM(String hostname) throws Exception;
	public abstract void rebootVM(String hostname) throws Exception;
	
	public RemoteExecutionResponse runRemoteCommand(JSONObject details , long timeout) throws Exception {
		RemoteExecutionResponse result = new RemoteExecutionResponse();
		int tries = 0;
		if(sshL!=null)
		{
			while(tries < SSH_TRIES_LIMIT){
				try{
					//System.out.println("executing command in cloud.java");
					result = sshL.runRemoteCommand(details,timeout);
					tries=SSH_TRIES_LIMIT;
				}catch(InterruptedException ie){
					throw new InterruptedException("Should not occur" + ie.getMessage());
				}catch(JSONException je){
					throw new JSONException("JSON Exception " + je.getMessage() + "\nThe JSON Object is :\n" +details.toString());
				}catch(Exception e){
					if(tries==SSH_TRIES_LIMIT-1)
						throw new java.net.ConnectException("Could not connect to the vm with host = "+details.getString("host")+ ". The ssh library returned this error : "+e.getMessage());
					else
						Thread.sleep(INTERVAL_BETWEEN_SSH_ATTEMPTS*1000);
				}
				tries++;
			}
		}
		//System.out.println("returning result of command from cloud.java");
		return result;
	};
	
	public RemoteExecutionResponse runRemoteCommand(JSONObject details) throws Exception {
		
		RemoteExecutionResponse result = new RemoteExecutionResponse();
		int tries = 0;
		if(sshL!=null)
		{
			while(tries < SSH_TRIES_LIMIT){
				try{
					//System.out.println("executing command in cloud.java");
					result = sshL.runRemoteCommand(details);
					tries=SSH_TRIES_LIMIT;
				}catch(InterruptedException ie){
					throw new InterruptedException("Should not occur" + ie.getMessage());
				}catch(JSONException je){
					throw new JSONException("JSON Exception " + je.getMessage() + "\nThe JSON Object is :\n" +details.toString());
				}catch(Exception e){
					if(tries==SSH_TRIES_LIMIT-1)
						throw new java.net.ConnectException("Could not connect to the vm with host = "+details.getString("host")+ ". The ssh library returned this error : "+e.getMessage());
					else
						Thread.sleep(INTERVAL_BETWEEN_SSH_ATTEMPTS*1000);
				}
				tries++;
			}
		}
		//System.out.println("returning result of command from cloud.java");
		return result;
	}
	public boolean uploadFile(JSONObject details) throws Exception{
		boolean result = false;
		int tries = 0;
		if(sshL!=null)
		{
			while(result==false && tries < SSH_TRIES_LIMIT){
				try{
					result = sshL.uploadFile(details);
					tries=SSH_TRIES_LIMIT;
				}catch(InterruptedException ie){
					// yet to decide what to do here
				}catch(JSONException je){
					throw new JSONException(je.getMessage());
				}catch(Exception e){
					if(tries==SSH_TRIES_LIMIT-1){
						throw new java.net.ConnectException("Could not connect to the vm and the json object given was :\n"+details.toString());
					}
					else
						Thread.sleep(INTERVAL_BETWEEN_SSH_ATTEMPTS*1000);
				}
				tries++;
			}
		}
		return result;
	}
	public boolean downloadFile(JSONObject details){
		boolean result = false;
		int tries = 0;
		if(sshL!=null)
		{
			while(result==false && tries < SSH_TRIES_LIMIT){
				result = sshL.downloadFile(details);
				try{
					Thread.sleep(INTERVAL_BETWEEN_SSH_ATTEMPTS*1000);
				}catch(Exception e){
					// yet to decide what to do here
				}
				tries++;
			}
		}
		return result;
	}
	
	protected abstract String extractInternalIP(VirtualMachine vm , String[] args) throws Exception;
	public abstract boolean setTags(VirtualMachine vm , JSONObject tags);
	public abstract void deleteTags(VirtualMachine vm) throws Exception;
	public abstract Map<String,String> getTags(VirtualMachine vm);
	protected abstract boolean loadMetadataOfExistingVMs() throws Exception;
	protected abstract void updateMetadataVM(VirtualMachine newVM , String sshKeyPath , String hostname) throws Exception;
	
}
