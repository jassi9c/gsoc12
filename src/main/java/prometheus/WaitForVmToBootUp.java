package prometheus;

import net.sf.json.JSONObject;

public class WaitForVmToBootUp extends CloudJob {

	private String ip;
	
	public WaitForVmToBootUp(Cloud cloud , JSONObject details) throws Exception {
		super(cloud,details);
		this.ip=cloud.getVirtualMachine(details.getString("hostname")).getPublicIp();
	}

	@Override
	public RemoteExecutionResponse execute() throws Exception {
		try{
			details.put("command", "sudo ls ");
			details.put("host", ip);
			return cloud.runRemoteCommand(details);			
		}catch(Exception e){
			throw new Exception(e.getMessage() + "\nThe JSON Object is :\n"+details.toString());
		}
	}

}
