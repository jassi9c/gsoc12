package prometheus;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.NotActiveException;
import java.io.OutputStream;

import org.omg.CORBA.RepositoryIdHelper;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import net.sf.json.JSONException;
import net.sf.json.JSONObject;

public class JschSSHLibrary extends SSHLibrary{

	private JSch jsch;
	
	@SuppressWarnings("static-access")
	public JschSSHLibrary(){
		jsch=new JSch();
		jsch.setConfig("StrictHostKeyChecking","no");
	}
	
	@Override
	RemoteExecutionResponse runRemoteCommand(JSONObject details) throws Exception{
		return runRemoteCommand(details,0);
	};
	
	@Override
	RemoteExecutionResponse runRemoteCommand(JSONObject details,long timeout) throws Exception{
		
		//String result = "";
		
		String host ;
		String username; 
		String sshKeyPath;
		String command;
		String port ; 
		
		RemoteExecutionResponse response = new RemoteExecutionResponse();
		
		try{
			host = details.getString("host");
			username = details.getString("username");
			sshKeyPath = details.getString("sshKeyPath");
			command = details.getString("command");
		}catch(JSONException je){
			throw new JSONException("host/username/key/command missing in json object.");
		}
				
		try {
			port = details.getString("port");
			Integer.parseInt(port);
		}catch(JSONException je){
			port = "22";
		}catch(NumberFormatException fme){
			throw new Exception(fme.getMessage());
		}
		
		try{
			jsch.addIdentity(new File(sshKeyPath).getCanonicalPath());
		}catch(JSchException je){
			throw new JSchException(je.getMessage());
		}
		
		//System.out.println("trying to execute the command now");
		boolean noTimeLimit = timeout==0?true:false;
		long startTime = 0;
		try{
			Session session=jsch.getSession(username, host, Integer.parseInt(port));
			session.connect();
			
			Channel channel=session.openChannel("exec");
			((ChannelExec)channel).setPty(true);
			((ChannelExec)channel).setCommand(command+" ; echo $?");
			channel.setInputStream(null);
			((ChannelExec)channel).setErrStream(System.err);
			InputStream in=channel.getInputStream();
			channel.connect();
			byte[] tmp=new byte[1024];
			
			startTime = System.currentTimeMillis();
			
			
			//System.out.println("noTimeLimit = "+noTimeLimit);
			
			while(noTimeLimit || System.currentTimeMillis() - startTime< timeout ){
				while(in.available()>0 && (noTimeLimit || System.currentTimeMillis() - startTime< timeout)){
					int i=in.read(tmp, 0, 1024);
					if(i<0)
						break;
					response.output+= new String(tmp, 0, i) ;
				}
				if(channel.isClosed()){
					break;
				}
			}
			channel.disconnect();
			session.disconnect();
		}catch(JSchException je){
			throw new JSchException(je.getMessage());
		}catch(java.net.ConnectException ce){
			throw new java.net.ConnectException(ce.getMessage());
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}	
		
		//System.out.println("command executed");
		
		if(noTimeLimit || System.currentTimeMillis() - startTime < timeout){
			try{
				if(!response.output.equals("")){
					String lines[] = response.output.split("\\r?\\n");
					response.output = response.output.substring(0,response.output.lastIndexOf(lines[lines.length-1]));
					response.returnStatus = Integer.parseInt(lines[lines.length-1]);
				}else{
					throw new Exception("Could not get the return status of remote execution.");
				}
			}catch(Exception e){
				e.printStackTrace();
				throw new Exception("Could not get the return status of remote execution.");
			}
		}else{ // timed out
			response.output = "Command Timeout occurred. The partial output is : \n"+response.output;
			response.returnStatus=1;
		}
		
		//System.out.println("returning the response");
		
		return response;
	}

	
	@Override
	boolean uploadFile(JSONObject details) throws Exception{
		boolean result = false;
		FileInputStream fis=null;
		
		String host ;
		String username; 
		String sshKeyPath;
		String sourceFile;
		String targetFile;
		String port ; 
		
		try{
			host = details.getString("host");
			username = details.getString("username");
			sshKeyPath = details.getString("sshKeyPath");
			sourceFile = details.getString("sourceFile");
			targetFile = details.getString("targetFile");
		}catch(JSONException je){
			throw new JSONException("host/username/key/sourceFile/targetFile missing in json object.");
		}
				
		try {
			port = details.getString("port");
			Integer.parseInt(port);
		}catch(JSONException je){
			port = "22";
		}catch(NumberFormatException fme){
			throw new Exception();
		}
		
		try{
			jsch.addIdentity(sshKeyPath);
			fis=new FileInputStream(sourceFile);
		}catch(JSchException je){
			throw new JSchException(je.getMessage());
		}catch(FileNotFoundException fnfe){
			throw new FileNotFoundException(fnfe.getMessage());
		}
		
		try{
			Session session=jsch.getSession(username, host, Integer.parseInt(port));
			session.connect();
			// -t option forces tty allocation , necessary for Amazon EC2 vms
	        String command="scp -t "+targetFile;
	        
	        Channel channel=session.openChannel("exec");
	        ((ChannelExec)channel).setCommand(command);
	        InputStream in=channel.getInputStream();
	        OutputStream out=channel.getOutputStream();
	        channel.connect();
	        
	        File sFile = new File(sourceFile);
	        long filesize = sFile.length();
	        command="C0644 "+filesize+" ";
	        if(sourceFile.lastIndexOf('/')>0){
	        	command+=sourceFile.substring(sourceFile.lastIndexOf('/')+1);
	        }
	        else{
	        	command+=sourceFile;
	        }
	        command+="\n";
	        out.write(command.getBytes()); 
	        out.flush();
	        if(checkAck(in)!=0){
	        	throw new Exception();
	        }
	        byte[] buf=new byte[1024];
	        while(true){
	        	int len=fis.read(buf, 0, buf.length);
	        	if(len<=0) 
	        		break;
	        	out.write(buf, 0, len); out.flush();
	        	if(checkAck(in)!=0){
	        		throw new Exception();
	        	}
	        }
	        fis.close();
	        fis=null;
	        // send '\0'
	        buf[0]=0; out.write(buf, 0, 1); out.flush();
	        if(checkAck(in)!=0){
	        	throw new Exception();
	        }
	        out.close();
	        channel.disconnect();
	        session.disconnect();
	        result=true;
		}catch(Exception e){
			throw new Exception();
		}
		return result;
	}

	@Override
	boolean downloadFile(JSONObject details) {
		// TODO Auto-generated method stub
		return false;
	}

	static int checkAck(InputStream in) throws IOException{
	    int b=in.read();
	    // b may be 0 for success,
	    //          1 for error,
	    //          2 for fatal error,
	    //          -1
	    if(b==0) return b;
	    if(b==-1) return b;

	    if(b==1 || b==2){
	      StringBuffer sb=new StringBuffer();
	      int c;
	      do {
		c=in.read();
		sb.append((char)c);
	      }
	      while(c!='\n');
	      if(b==1){ // error
		System.out.print(sb.toString());
	      }
	      if(b==2){ // fatal error
		System.out.print(sb.toString());
	      }
	    }
	    return b;
	  }

	
}
