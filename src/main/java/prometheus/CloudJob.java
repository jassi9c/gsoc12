package prometheus;

import net.sf.json.JSONException;
import net.sf.json.JSONObject;

public abstract class CloudJob {

	Object result ;
	JSONObject details ;
	Cloud cloud ;
	
	public CloudJob(){
		details=null;
		cloud=null;
		result=null;
	}
	
	@SuppressWarnings("unused")
	public CloudJob(Cloud cloud , JSONObject details) throws Exception{
		
		String username ; 
		String sshKeyPath ;  
		String host ;
		try{
			username = details.getString("username");
			sshKeyPath = details.getString("sshKeyPath");
		}catch(JSONException je){
			throw new JSONException("username/sshKeyPath missing in json object.");
		}
		this.details=details;
		this.cloud=cloud;
	}
	
	void setCloud(Cloud cloud){
		this.cloud=cloud;
	}
	
	@SuppressWarnings("unused")
	void setDetails(JSONObject details){
		String username ; 
		String sshKeyPath ;  
		String host ;
		try{
			username = details.getString("username");
			sshKeyPath = details.getString("sshKeyPath");
		}catch(JSONException je){
			throw new JSONException("username/sshKeyPath missing in json object.");
		}
		this.details=details;
	}
	
	public abstract RemoteExecutionResponse execute() throws Exception;
	
	public Object getResult(){
		return result;
	}
	
}
