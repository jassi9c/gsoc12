package prometheus;

import net.sf.json.JSONObject;

public abstract class SSHLibrary {
	
	abstract RemoteExecutionResponse runRemoteCommand(JSONObject details) throws Exception;
	abstract RemoteExecutionResponse runRemoteCommand(JSONObject details,long timeout) throws Exception;
	abstract boolean uploadFile(JSONObject details) throws Exception;
	abstract boolean downloadFile(JSONObject details);

}
