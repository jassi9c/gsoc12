package prometheus;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import net.sf.json.JSONObject;

public class VirtualMachine {
	
	private String privateIp;
	private String publicIp;
	private Map<String,String> tags;
	private Object vm ; 
	private String hostname;
	private String[] dnsServers;
	private String status;
	private boolean isDNSServer;
	private String keyName;
	private String key;
	
	public VirtualMachine(){
		privateIp=null;
		publicIp=null;
		tags=null;
		vm=null;
		hostname=null;
		status=null;
		dnsServers=null;
		isDNSServer=false;
	}
	
	public VirtualMachine(VirtualMachine vm){
		this.vm=vm;
	}
	
	public VirtualMachine(String privateIp,String publicIp,Map<String,String> tags,Object vm,String hostname,String status){
		this.privateIp=privateIp;
		this.publicIp=publicIp;
		this.tags=tags;
		this.vm=vm;
		this.hostname=hostname;
		this.status=status;
	}
	
	public String getKeyName(){
		return keyName;
	}
	
	public void setKeyName(String name){
		this.keyName=name;
	}
	
	public void setKey(String key){
		this.key=key;
	}
	
	public String getKey(){
		return key;
	}
	
	public String getPrivateIp(){
		return privateIp;
	}
	public void setPrivateIp(String ip){
		privateIp=ip;
	}
	
	public String getPublicIp(){
		return publicIp;
	}
	public void setPublicIp(String ip){
		publicIp=ip;
	}
	
	public Map<String,String> getTags(){
		return tags;
	}
	public void setTags(Map<String,String> tags){
		this.tags=tags;
	}
	public void setTags(JSONObject tags){
		try{
		Iterator keys = tags.keys();
		Map<String,String> temp = new HashMap<String, String>();
		while(keys.hasNext()){
			String key = keys.next().toString();
			temp.put(key,tags.getString(key));
		}
		this.tags=temp;
		}catch(Exception e){
			System.out.println(getHostname()+" : Exception occurred in setTags() :"+e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	public Object getVM(){
		return vm;
	}
	public void setVM(Object vm){
		this.vm=vm;
	}
	
	public String getHostname(){
		return hostname;
	}
	public void setHostname(String hostname){
		this.hostname=hostname;
	}
	
	public String[] getListOfDNSServers(){
		return dnsServers;
	}
	public void setListOfDNSServers(String[] servers){
		dnsServers=servers;
	}
	
	public String getStatus(){
		return status;
	}
	public void setStatus(String status){
		this.status=status;
	}
	
	public boolean isThisADNSServer(){
		return isDNSServer;
	}
	public void setAsDNSServer(boolean value){
		isDNSServer=value;
	}
}
