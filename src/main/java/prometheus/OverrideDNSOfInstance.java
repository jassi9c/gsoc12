package prometheus;

import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;

public class OverrideDNSOfInstance extends CloudJob{

	private String ip;
	
	public OverrideDNSOfInstance(Cloud cloud , JSONObject details) throws Exception{
		super(cloud,details);
		this.ip=cloud.getVirtualMachine(details.getString("hostname")).getPublicIp();
	}
	
	@Override
	public RemoteExecutionResponse execute() throws Exception {
		
		JSONArray nameservers;
		try{
			nameservers = details.getJSONArray("overrideDNS");
		}catch(JSONException je){
			throw new JSONException("nameserver missing in this json object :" + details.toString());
		}
		try{
			String command = "";
			String defaultNameserver = "";
			boolean getDefaultNameserver=false;
			for(int i=0;i<nameservers.size();i++){
				String ns = nameservers.getString(i);
				if(ns.equals("default"))
					getDefaultNameserver=true;
				else if(cloud.getVirtualMachine(ns).getPrivateIp()==null)
					throw new JSONException("The nameserver \""+ns+"\" in the json object does not exist");
			}
			
			if(getDefaultNameserver){
				command = "cat /etc/resolv.conf | grep nameserver | cut -f2 -d ' '";
				details.put("command",command);
				details.put("host", ip);
				defaultNameserver = cloud.runRemoteCommand(details).getOutput();
				System.out.println("default nameserver is : "+defaultNameserver);
			}
			
			String ns = nameservers.getString(0);
			ns = ns.equals("default")?defaultNameserver:cloud.getVirtualMachine(ns).getPrivateIp();
			command = "sudo rm -rf /etc/resolv.conf ; sudo touch /etc/resolv.conf ; sudo sh -c 'echo \"nameserver "+ns+"\" > /etc/resolv.conf'"; 
			for(int i=1 ; i<nameservers.size() ; i++){
				ns = nameservers.getString(i);
				ns = ns.equals("default")?defaultNameserver:cloud.getVirtualMachine(ns).getPrivateIp();
				command+= "; sudo sh -c ' echo \"nameserver "+ns+"\" >> /etc/resolv.conf'";
			}
			details.put("command",command);
			details.put("host", ip);
			return cloud.runRemoteCommand(details);
			/*
			String ns = nameservers.getString(0);
			ns = cloud.getVirtualMachine(ns).getPrivateIp();
			if(ns==null)
				throw new JSONException("The nameserver \""+ns+"\" in the json object does not exist");
			
			
			if(details.containsKey("isDNSServer")==false){
				command = "sudo rm -rf /etc/resolv.conf ; sudo touch /etc/resolv.conf ; sudo sh -c 'echo \"nameserver "+ns+"\" > /etc/resolv.conf'"; 
				for(int i=1 ; i<nameservers.size() ; i++){
					ns = nameservers.getString(i);
					ns = cloud.getVirtualMachine(ns).getPrivateIp();
					command+= "; sudo sh -c ' echo \"nameserver "+ns+"\" >> /etc/resolv.conf'";
				}
			}else{ // add nameserver ip to itself , to allow resolution of ips from itself also
				command+= "sudo sh -c ' echo \"nameserver "+ns+"\" >> /etc/resolv.conf'";
			}
			
			details.put("command",command);
			details.put("host", ip);
			return cloud.runRemoteCommand(details);
			*/
		}catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}

}
