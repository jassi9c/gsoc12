package prometheus;

public class RemoteExecutionResponse {

	String output;
	int returnStatus;

	public RemoteExecutionResponse(){
		output = "";
		returnStatus=-1;
	}
	
	public String getOutput(){
		return output;
	}
	
	public int getReturnStatus(){
		return returnStatus;
	}
	
}
