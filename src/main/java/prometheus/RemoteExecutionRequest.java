package prometheus;

import net.sf.json.JSONObject;

public class RemoteExecutionRequest implements Runnable {

	private Cloud cloud;
	private JSONObject request;
	private RemoteExecutionResponse response ;
	public RemoteExecutionRequest(JSONObject request , Cloud cloud) throws Exception{
		this.cloud = cloud;
		this.request = request;
	}

	public void run() {
		
		try{
			String hostname = request.getString("hostname");
			if(request.getString("type").equals("command")){
				if(cloud.getVirtualMachine(hostname).getPublicIp()!=null)
					request.put("host", cloud.getVirtualMachine(hostname).getPublicIp());
				else{
					System.out.println("Could not get the external ip of the vm");
					//System.out.println(cloud.getExternalIP(cloud.getVM(hostname)));
					return;
				}
				response = cloud.runRemoteCommand(request);
				if(checkResponse(response))
					return;
				System.out.println("The output for command \""+request.getString("command")+"\" on host \""+request.getString("host")+"\" is : \n"+response.getOutput());
			}
			else if(request.getString("type").equals("script")){
				RunScriptOnInstance runSriptReq = new RunScriptOnInstance(cloud, request);
				response = runSriptReq.execute();
				if(checkResponse(response))
					return;
				System.out.println("The output for script \""+request.getString("script")+"\" on host \""+hostname+"\"is : \n"+response.getOutput());
			}else if(request.getString("type").equals("start_vm")){
				VirtualMachine vm = cloud.startVM(hostname);
				WaitForVmToBootUp waitReq = new WaitForVmToBootUp(cloud,request);
				//RemoteExecutionResponse waitReqResponse = waitReq.execute();
				waitReq.execute();
				// add logic to update ip in dns
				
				cloud.updateMetadataVM(vm, request.getString("sshKeyPath"), hostname);
				System.out.println("VM with hostname \""+hostname+"\" started successfully.\n");
			}else if(request.getString("type").equals("stop_vm")){
				cloud.stopVM(hostname);
				System.out.println("VM with hostname \""+hostname+"\" stopped successfully.\n");
			}else if(request.getString("type").equals("terminate_vm")){
				cloud.terminateVM(hostname);
				System.out.println("VM with hostname \""+hostname+"\" terminated successfully.\n");
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
			if(e.getMessage()==null)
				e.printStackTrace();
		}
	}
	
private boolean checkResponse(RemoteExecutionResponse response){
		
		if(response.returnStatus!=0){
			System.out.println("Error : ");
			System.out.println("Shell response : \n"+response.getOutput());
			return true;
		}
		return false;
		
	}
	

}
