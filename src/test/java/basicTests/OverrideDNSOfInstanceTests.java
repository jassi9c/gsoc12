package basicTests;

import static org.junit.Assert.*;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.junit.Before;
import org.junit.Test;

import prometheus.AmazonCloud;
import prometheus.Cloud;
import prometheus.CloudJob;
import prometheus.OverrideDNSOfInstance;
import prometheus.RemoteExecutionResponse;

public class OverrideDNSOfInstanceTests {

	Cloud cloud;
	JSONObject json ;
	Object testvm;
	
	
	@Before
	public void setUpBeforeClass(){
		
		try{
	    cloud = new AmazonCloud(System.getProperty("user.dir")+"/src/test/resources/awsCredentials/AwsCredentials.properties");
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	    
		json = new JSONObject();
		
		try{
	    json.put("ami", "ami-e565ba8c");
		json.put("type" , "t1.micro");
		json.put("securityGroup" , "test");
		json.put("sshKey" , "gsoc_dns");
		json.put("sshKeyPath", System.getProperty("user.dir")+"/src/test/resources/gsoc_dns.pem");
		testvm = cloud.launchVM(json);
		}catch(Exception e){
			System.out.println(e.getMessage());			
		}
				
		if(testvm==null)
			fail("Could not launch vm for testing.");
		
	}
	
	@Test
	public void testBasicOverride() throws Exception{
		JSONArray nameservers = new JSONArray();
		nameservers.add("10.2.3.4");
		json.put("overrideDNS", nameservers);
		json.put("username", "ec2-user");
		//json.put("host", );
		json.put("sshKey" , "gsoc_dns");
		json.put("sshKeyPath", System.getProperty("user.dir")+"/src/test/resources/gsoc_dns.pem");
		try{
			CloudJob overridedns = new OverrideDNSOfInstance(cloud, json );
			RemoteExecutionResponse output = overridedns.execute();
			assert(output.getReturnStatus()==0);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

}
