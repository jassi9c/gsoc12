package basicTests;

import static org.junit.Assert.*;

import net.sf.json.JSONException;
import net.sf.json.JSONObject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import prometheus.AmazonCloud;
import prometheus.Cloud;
import prometheus.RemoteExecutionResponse;

public class JSchSSHLibraryTests {

	Cloud cloud;
	JSONObject json ; 
	
	@Before
	public void setUpBeforeClass() throws Exception {

		
		cloud = new AmazonCloud();
		json = new JSONObject();
		json.put("host", "web.iiit.ac.in");
		json.put("username", "jaspal.dhillonug08");
		json.put("sshkey", "id_rsa.pub");
		json.put("command", "cat test");
		json.put("port", "22");
	}

	@After
	public void tearDown() throws Exception {
			cloud = null;
			json=null;
	}
	
	
	@Test(expected=JSONException.class)
	public void testMissingHost() throws Exception{
		json.remove("host");
		cloud.runRemoteCommand(json);
	}
	@Test(expected=JSONException.class)
	public void testMissingUsername() throws Exception{
		json.remove("username");
		cloud.runRemoteCommand(json);
	}	
	@Test(expected=JSONException.class)
	public void testMissingKey() throws Exception{
		json.remove("sshkey");
		cloud.runRemoteCommand(json);
	}
	@Test(expected=JSONException.class)
	public void testMissingCommand() throws Exception{
		json.remove("command");
		cloud.runRemoteCommand(json);
	}
	@Test(expected=Exception.class)
	public void testNonExistentSSHKey() throws Exception{
		cloud.runRemoteCommand(json);
	}
	//@Test
	public void testCommandOnMyWebServer() throws Exception{
		json.remove("sshkey");
		json.put("sshkey", "/home/jaspal/.ssh/id_rsa");
		RemoteExecutionResponse output = cloud.runRemoteCommand(json);
		assert(output.getReturnStatus()!=0);
	}
	
	@Test
	public void testCommandOnAmazonEc2AsNormalUser() throws Exception{
		json.remove("sshkey");
		json.remove("username");
		json.remove("host");
				
		json.put("command", "cat test");
		json.put("sshkeypath", System.getProperty("user.dir")+"/src/test/resources/gsoc_dns.pem");
		json.put("host", "ec2-23-21-12-252.compute-1.amazonaws.com");
		json.put("username", "ec2-user");
	
		RemoteExecutionResponse output = cloud.runRemoteCommand(json);
		assert(output.getReturnStatus()!=0);
	}
	@Test
	public void testCommandOnAmazonEc2AsSuperUser() throws Exception{
		
		json.remove("sshkey");
		json.remove("username");
		json.remove("host");
		json.remove("command");
		
		json.put("command", "sudo tail -10 /var/log/messages");
		json.put("sshkeypath", System.getProperty("user.dir")+"/src/test/resources/gsoc_dns.pem");
		json.put("host", "ec2-23-21-12-252.compute-1.amazonaws.com");
		json.put("username", "ec2-user");
	
		RemoteExecutionResponse output = cloud.runRemoteCommand(json);
		assert(output.getReturnStatus()!=0);
		System.out.println(output.getOutput());
	}
	@Test
	public void testUploadingFileToMyWebServer() throws Exception{
		json.remove("sshkey");
		json.put("sshkeypath", "/home/jaspal/.ssh/id_rsa");
		json.put("sourceFile", "/home/jaspal/test");
		json.put("targetFile", "~/old/test");
		assertTrue("This should be true",cloud.uploadFile(json));
	}
	@Test
	public void testUploadFileOnAmazonEc2AsNormalUser() throws Exception {
		json.remove("sshkey");
		json.remove("username");
		json.remove("host");
		
		json.put("sshkeypath", System.getProperty("user.dir")+"/src/test/resources/gsoc_dns.pem");
		json.put("host", "ec2-23-21-12-252.compute-1.amazonaws.com");
		json.put("username", "ec2-user");
		json.put("sourceFile", "/home/jaspal/test");
		json.put("targetFile", "~/test");
		assertTrue("This should be true",cloud.uploadFile(json));

	}
	
}
