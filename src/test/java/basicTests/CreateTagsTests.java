package basicTests;


import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.sf.json.JSONObject;

import org.junit.Before;
import org.junit.Test;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.Tag;

public class CreateTagsTests {

	AmazonEC2 cloud;
	JSONObject json ;
	String id = "";
	@Before
	public void setUpBeforeClass(){
		
		try{
	    //cloud = new AmazonCloud(System.getProperty("user.dir")+"/src/test/resources/awsCredentials/AwsCredentials.properties");
	    AWSCredentials credentials = new PropertiesCredentials(new FileInputStream(new File(System.getProperty("user.dir")+"/src/test/resources/awsCredentials/AwsCredentials.properties")));
	 	cloud = new AmazonEC2Client(credentials);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	    
		json = new JSONObject();
		
		try{
	    json.put("ami", "ami-e565ba8c");
		json.put("type" , "t1.micro");
		json.put("securityGroup" , "test");
		json.put("sshKey" , "gsoc_dns");
		json.put("sshKeyPath", System.getProperty("user.dir")+"/src/test/resources/gsoc_dns.pem");
		
		id = "i-204f8158";
		
		}catch(Exception e){
			System.out.println(e.getMessage());			
		}
	}
	
	
	@Test
	public void test() {
		
		try{

			List<String> ids = new ArrayList<String>();
			ids.add(id);


			List<Tag> vmtags = new ArrayList<Tag>();
			@SuppressWarnings("rawtypes")
			Iterator keys = json.keys();
			while(keys.hasNext()){
				Tag t = new Tag();
				t.setKey((String)keys.next());
				t.setValue(json.getString(t.getKey()));
				vmtags.add(t);
			}
			CreateTagsRequest ctr = new CreateTagsRequest();
			ctr.setTags(vmtags);
			ctr.setResources(ids);
			cloud.createTags(ctr);
			System.out.println(ctr.toString());


		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

}
