package basicTests;

import static org.junit.Assert.*;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.junit.Test;

import prometheus.AddEntryToDNSServers;
import prometheus.AmazonCloud;
import prometheus.Cloud;
import prometheus.CloudJob;
import prometheus.RemoteExecutionResponse;

@SuppressWarnings("unused")
public class AddEntryToDNSServersTests {

	@Test
	public void testBasic() {
		
		Cloud cloud = null;
		JSONObject json = null;
		
		try{
			
			cloud = new AmazonCloud(System.getProperty("user.dir")+"/src/test/resources/awsCredentials/AwsCredentials.properties");
			//cloud.setDNSServer("testDNS", "50.16.71.27");
			json = new JSONObject();
			json.put("ami", "ami-e565ba8c");
			json.put("type" , "t1.micro");
			json.put("securityGroup" , "test");
			json.put("sshKey" , "gsoc_dns");
			json.put("sshKeyPath", System.getProperty("user.dir")+"/src/test/resources/gsoc_dns.pem");
			json.put("username", "ec2-user");

			JSONArray servers = new JSONArray();
			JSONObject theserver = new JSONObject();
			theserver.put("dnsServer", "testDNS");
			theserver.put("dnsServerIp", "22.22.2.2");
			theserver.put("dnsFile", "/var/named/emory.edu");
			servers.add(theserver);
			
			json.put("addEntryToDNSServers", servers);
			json.put("hostname", "test2.emory.edu");
			
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		try{
			CloudJob addE = new AddEntryToDNSServers(cloud , json );
			RemoteExecutionResponse res = addE.execute();
			
			if(res.getReturnStatus()!=0)
				System.out.println(res.getOutput());
			
		}catch(Exception e){
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

}
