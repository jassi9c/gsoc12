package basicTests;


import static org.junit.Assert.*;


import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import com.amazonaws.AmazonClientException;

import prometheus.*;
import java.io.FileNotFoundException;

@SuppressWarnings("unused")
public class AmazonCloudConnectionTests {

	@Test(expected=FileNotFoundException.class)
	public void testNonExistentCredentialsFileInInit() throws Exception{
		AmazonCloud amazonCloud = new AmazonCloud();
		amazonCloud.init("path/to/non_existent/file");
	}
	
	@Test(expected=FileNotFoundException.class)
	public void testNonExistentCredentialsFileInConstructor() throws Exception{
		AmazonCloud amazonCloud = new AmazonCloud("path/to/non_existent/file");
	}
	
	@Test(expected=FileNotFoundException.class)
	public void testCorrectCredentialsFileWrongPermission() throws Exception{
		AmazonCloud amazonCloud = new AmazonCloud(System.getProperty("user.dir")+"/src/test/resources/awsCredentials/AwsCredentialsIncorrectPerms.properties");	
	}

	@Rule
	public ExpectedException expectedEx = ExpectedException.none();
	
	@Test
	public void testIncorrectCredentials() throws Exception {
		expectedEx.expect(AmazonClientException.class);
		//expectedEx.expectMessage("Invalid Credentials");
		AmazonCloud amazonCloud = new AmazonCloud(System.getProperty("user.dir")+"/src/test/resources/awsCredentials/IncorrectAwsCredentials.properties");
	}
	
	@Test
	public void testCorrectCredentials() throws Exception{
		Cloud amazonCloud = new AmazonCloud("./src/test/resources/awsCredentials/AwsCredentials.properties");
		assert(amazonCloud!=null);
	}
	
	@Test
	public void testCorrectCredentialsAsIAMUser() throws Exception{
		Cloud amazonCloud = new AmazonCloud(System.getProperty("user.dir")+"/src/test/resources/awsCredentials/AwsCredentials_IAM.properties");
		assert(amazonCloud!=null);
	}
}
